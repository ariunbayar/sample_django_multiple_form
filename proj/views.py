from django.shortcuts import render
from .models import Project
from .forms import BedForm


def add(request):

    if request.method == 'POST':
        forms = [
                BedForm(request.POST, prefix='f1'),
                BedForm(request.POST, prefix='f2'),
                BedForm(request.POST, prefix='f3'),
                BedForm(request.POST, prefix='f4'),
            ]
        if all([f.is_valid() for f in forms]):

            project, is_created = Project.objects.get_or_create(name='Project 1')

            form1, form2, form3, form4 = forms

            form1.instance.kind = 'surgery-or-abdomen'
            form1.instance.project = project
            form1.save()

            form2.instance.kind = 'maternity'
            form2.instance.project = project
            form2.save()

            form3.instance.kind = 'premature'
            form3.instance.project = project
            form3.save()

            form4.instance.kind = 'other'
            form4.instance.project = project
            form4.save()

    else:
        forms = [
                BedForm(prefix='f1'),
                BedForm(prefix='f2'),
                BedForm(prefix='f3'),
                BedForm(prefix='f4'),
            ]

    context = {
            'forms': forms,
        }

    return render(request, 'proj/add.html', context)
