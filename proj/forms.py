from django import forms
from .models import Bed


class BedForm(forms.ModelForm):

    class Meta:

        model = Bed

        fields = [
            'current',
            'increment',
            'decrement',
            'final',
        ]

        labels = {
            }

        widgets = {
            }

        error_messages = {
            }

        help_texts = {
            }
