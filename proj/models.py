from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=250)


class Bed(models.Model):
    project = models.ForeignKey(Project, on_delete=models.PROTECT)

    KIND_CHOICES = [
        ('surgery-or-abdomen', 'Дотор / Мэс засал'),
        ('maternity', 'Эх барих'),
        ('premature', 'Хүүхэд'),
        ('other', 'Бусад'),
    ]
    kind = models.CharField(max_length=20, choices=KIND_CHOICES)

    current = models.PositiveIntegerField()
    increment = models.PositiveIntegerField()
    decrement = models.PositiveIntegerField()
    final = models.PositiveIntegerField()
